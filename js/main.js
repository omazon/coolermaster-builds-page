jQuery(document).ready(function ($) {
    $('.owl-one').owlCarousel({
        loop: true,
        nav: true,
        autoplay: true,
        margin:10,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        },
        dots:false,
        navText:['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
        navClass:['nav-class owl-prev','nav-class owl-next']
    });
    $('.owl-two').owlCarousel({
        loop: true,
        autoplay: true,
        nav: true,
        margin:10,
        navText:['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
        navClass:['nav-class owl-prev','nav-class owl-next'],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            }
        },
        dots:false
    });
    $('#pc-parts').DataTable({
        paging: false,
        info: false,
        lengthChange: false
    });
    $(window).on('load',function () {
        $('.preloader').fadeOut('slow');
    });
});
